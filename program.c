#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int is_prime(int num) {
    if (num <= 1) return 0;
    if (num <= 3) return 1;
    if (num % 2 == 0 || num % 3 == 0) return 0;
    for (int i = 5; i * i <= num; i += 6) {
        if (num % i == 0 || num % (i + 2) == 0) return 0;
    }
    return 1;
}

int main(int argc, char** argv) {
    int rank, size;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (argc != 2) {
        if (rank == 0) {
            fprintf(stderr, "Usage: %s <number>\n", argv[0]);
        }
        MPI_Finalize();
        return EXIT_FAILURE;
    }

    int n = atoi(argv[1]);
    int original_n = n;

    // Generate primes array
    int* primes = (int*)malloc((n + 1) * sizeof(int));
    int prime_count = 0;
    for (int i = 2; i <= n; i++) {
        if (is_prime(i)) {
            primes[prime_count++] = i;
        }
    }

    // Each process handles a prime factor if available
    for (int i = rank; i < prime_count; i += size) {
        int my_prime = primes[i];
        while (n > 1 && n % my_prime == 0) {
            printf("Process %d: %d is divisible by %d\n", rank, original_n, my_prime);
            n /= my_prime;
        }
    }

    // Communication to ensure all processes have finished factorization
    MPI_Barrier(MPI_COMM_WORLD);

    // Communication loop to send the remaining number to the next process
    int next_rank = (rank + 1) % size;
    int prev_rank = (rank - 1 + size) % size;

    MPI_Send(&n, 1, MPI_INT, next_rank, 0, MPI_COMM_WORLD);
    MPI_Recv(&n, 1, MPI_INT, prev_rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    free(primes);
    MPI_Finalize();
    return EXIT_SUCCESS;
}
