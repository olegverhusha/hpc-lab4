#!/bin/bash

#PBS -N mpi_program
#PBS -l nodes=1:ppn=1
#PBS -l walltime=00:02:00

ml icc
ml openmpi

cd $PBS_O_WORKDIR

# Запуск MPI-програми з використанням mpirun
mpirun ./program 11
